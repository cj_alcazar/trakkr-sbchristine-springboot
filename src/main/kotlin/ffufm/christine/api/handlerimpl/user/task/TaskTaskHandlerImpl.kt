package ffufm.christine.api.handlerimpl.task

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.christine.api.repositories.task.TaskTaskRepository
import ffufm.christine.api.repositories.user.user.UserUserRepository
import ffufm.christine.api.spec.dbo.task.TaskTask
import ffufm.christine.api.spec.dbo.task.TaskTaskDTO
import ffufm.christine.api.spec.handler.task.TaskTaskDatabaseHandler

import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException


@Component("task.TaskTaskHandler")
class TaskTaskHandlerImpl(
    private val userRepository: UserUserRepository,
    private val projectRepository: TaskTaskRepository
) : PassDatabaseHandler<TaskTask, TaskTaskRepository>(),
    TaskTaskDatabaseHandler {
    override suspend fun create(body: TaskTaskDTO, id: Long, projectId: Long): TaskTaskDTO {
        val bodyEntity = body.toEntity()
        val assignee = userRepository.findById(id).orElseThrow404(id)
        val works = projectRepository.findById(projectId).orElseThrow404(projectId)

        return repository.save(bodyEntity.copy(
            assignee = assignee,
           taskProjectRelationship = works.taskProjectRelationship )).toDto()
    }

    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)

        return repository.delete(original)
    }

    override suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO {
        val bodyEntity = body.toEntity()
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(
            original.copy(
                name = bodyEntity.name,
                description = bodyEntity.description,
                status = bodyEntity.status
            )
        ).toDto()
    }

}