package ffufm.christine.api.handlerimpl.user.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.christine.api.repositories.user.user.UserAddressRepository
import ffufm.christine.api.spec.dbo.user.UserAddress
import ffufm.christine.api.spec.dbo.user.UserAddressDTO
import ffufm.christine.api.spec.handler.user.UserAddressDatabaseHandler
import kotlin.Long
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserAddressHandler")
class UserAddressHandlerImpl : PassDatabaseHandler<UserAddress, UserAddressRepository>(),
        UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: Delete is successful.
     * HTTP Code 404: The object with the ID is not found
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    override suspend fun update(body: UserAddressDTO, id: Long): UserAddressDTO {
        val bodyEntity = body.toEntity()

        val address = repository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "Address with id: $id does not exist")
        }

        return repository.save(address.copy(
            street = bodyEntity.street,
            barangay = bodyEntity.barangay,
            city = bodyEntity.city,
            province = bodyEntity.province,
            zipCode = bodyEntity.zipCode
        )).toDto()
    }

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    override suspend fun createAddress(body: UserAddressDTO, id: Long): UserAddressDTO {
        val bodyEntity = body.toEntity()

        if(repository.doesAddressExist(bodyEntity.street, bodyEntity.barangay, bodyEntity.city, bodyEntity.province, bodyEntity.zipCode)){
            throw ResponseStatusException(
                HttpStatus.CONFLICT, "Address ${bodyEntity.street}, ${bodyEntity.barangay}, ${bodyEntity.city}" +
                        ", ${bodyEntity.province}, ${bodyEntity.zipCode}, already exists")
        }

        return repository.save<UserAddress?>(body.toEntity()).toDto()
    }
}
