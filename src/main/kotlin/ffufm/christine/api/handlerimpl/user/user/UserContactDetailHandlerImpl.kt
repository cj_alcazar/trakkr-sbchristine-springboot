package ffufm.christine.api.handlerimpl.user.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import ffufm.christine.api.repositories.user.user.UserContactDetailRepository
import ffufm.christine.api.spec.dbo.user.UserContactDetail
import ffufm.christine.api.spec.dbo.user.UserContactDetailDTO
import ffufm.christine.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.christine.api.utils.Constants
import kotlin.Long
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserContactDetailHandler")
class UserContactDetailHandlerImpl : PassDatabaseHandler<UserContactDetail,
        UserContactDetailRepository>(), UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: Object with the ID is not found
     */
    override suspend fun createContactDetail(body: UserContactDetailDTO, id: Long):
            UserContactDetailDTO {
        val bodyEntity = body.toEntity()

        //To check if the contact details is already existing in the db.
        if (repository.doesContactDetailExist(contactDetails = bodyEntity.contactDetails)) {
            throw ResponseStatusException(
                HttpStatus.CONFLICT,
                "Contact Detail ${body.contactDetails} already exists!"
            )
        }

        // Check if it's the 4th one
        // get all contact details by userId
        if (repository.countContactDetailsByUserId(id) >= Constants.NAX_CONTACT_DETAILS) {
            throw ResponseStatusException(HttpStatus.BAD_REQUEST, "You cannot create more than four details.")
        }

        if (bodyEntity.isPrimary) {
            // Create a query that will update the value of all contactDetails isPrimary=false
            repository.updateIsPrimaryToFalse(id)
        }
        return repository.save(body.toEntity()).toDto()
    }

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    override suspend fun updateContactDetails(body: UserContactDetailDTO, id: Long):
            UserContactDetailDTO {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.save(original).toDto()
    }

    /**
     * : 
     * HTTP Code 200: Delete is successful.
     * HTTP Code 404: The object with the ID is not found
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }
}
