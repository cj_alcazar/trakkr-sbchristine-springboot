package ffufm.christine.api.handlerimpl.user.user

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.christine.api.repositories.user.user.UserUserRepository
import ffufm.christine.api.spec.dbo.user.UserUser
import ffufm.christine.api.spec.dbo.user.UserUserDTO
import ffufm.christine.api.spec.handler.user.UserUserDatabaseHandler
import ffufm.christine.api.utils.UserTypeEnum
import kotlin.Int
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("user.UserUserHandler")
class UserUserHandlerImpl : PassDatabaseHandler<UserUser, UserUserRepository>(),
        UserUserDatabaseHandler {
    /**
     * Create User: Creates a new User object
     * HTTP Code 201: The created User
     */
    override suspend fun create(body: UserUserDTO): UserUserDTO {

        val bodyEntity = body.toEntity()

        // Check if the email already exist in the db
        // Create a custom query
        if (repository.doesEmailExist(bodyEntity.email))
            throw ResponseStatusException(HttpStatus.CONFLICT, "Email ${body.email} already exist.")
        //save the user using the  UserRepository
        val userType = UserTypeEnum.valueOf(bodyEntity.userType.uppercase())

        //save the user using the  UserRepository
        return repository.save(bodyEntity.copy(
            userType = userType.value)).toDto()
    }

    /**
     * Finds Users by ID: Returns Users based on ID
     * HTTP Code 200: The User object
     * HTTP Code 404: A object with the submitted ID does not exist!
     */
    override suspend fun getById(id: Long): UserUserDTO? {
        return repository.findById(id).orElseThrow404(id).toDto()
    }

    /**
     * Get all Users by page: Returns all Users from the system that the user has access to. The
     * Headers will include TotalElements, TotalPages, CurrentPage and PerPage to help with Pagination.
     * HTTP Code 200: List of Users
     */
    override suspend fun getAll(maxResults: Int, page: Int): Page<UserUserDTO> {

        return repository.findAll(Pageable.unpaged()).toDtos()
    }

    /**
     * Update the User: Updates an existing User
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    override suspend fun update(body: UserUserDTO, id: Long): UserUserDTO {
        val original = repository.findById(id).orElseThrow404(id)
        val bodyEntity = body.toEntity()
        return repository.save(original.copy(
            firstName = bodyEntity.firstName,
            lastName = bodyEntity.lastName,
            email = bodyEntity.email
        )).toDto()
    }

    /**
     * Delete User by id.: Deletes one specific User.
     */
    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }
}
