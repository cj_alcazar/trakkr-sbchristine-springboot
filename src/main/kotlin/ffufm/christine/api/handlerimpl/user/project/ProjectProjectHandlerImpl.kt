package ffufm.christine.api.handlerimpl.project

import de.ffuf.pass.common.handlers.PassDatabaseHandler
import de.ffuf.pass.common.utilities.extensions.orElseThrow404
import de.ffuf.pass.common.utilities.extensions.toDtos
import ffufm.christine.api.repositories.project.ProjectProjectRepository
import ffufm.christine.api.repositories.user.user.UserUserRepository
import ffufm.christine.api.spec.dbo.project.ProjectProject
import ffufm.christine.api.spec.dbo.project.ProjectProjectDTO
import ffufm.christine.api.spec.handler.project.ProjectProjectDatabaseHandler

import org.springframework.data.domain.Page
import org.springframework.data.domain.PageRequest

import org.springframework.http.HttpStatus
import org.springframework.stereotype.Component
import org.springframework.web.server.ResponseStatusException

@Component("project.ProjectProjectHandler")
class ProjectProjectHandlerImpl  ( private val userRepository: UserUserRepository): PassDatabaseHandler<ProjectProject, ProjectProjectRepository>(),
    ProjectProjectDatabaseHandler {


    override suspend fun create(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val bodyEntity = body.toEntity()
        val user = userRepository.findById(id).orElseThrow {
            ResponseStatusException(HttpStatus.NOT_FOUND, "User with id: $id not found")
        }

        return repository.save(bodyEntity.copy( owner = user )).toDto()
    }



    override suspend fun getAll(maxResults: Int, page: Int): Page<ProjectProjectDTO> {
        val pagination = PageRequest.of(page ?: 0, maxResults ?: 100)
        return repository.findAll(pagination).toDtos()
    }

    override suspend fun update(body: ProjectProjectDTO, id: Long): ProjectProjectDTO {
        val bodyEntity = body.toEntity()
        val original = repository.findById(id).orElseThrow404(id)

        return repository.save(original.copy(
            name = bodyEntity.name,
            description = bodyEntity.description,
            status = bodyEntity.status
        )).toDto()
    }



    override suspend fun remove(id: Long) {
        val original = repository.findById(id).orElseThrow404(id)
        return repository.delete(original)
    }


}