package ffufm.christine.api.repositories.user.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.christine.api.spec.dbo.user.UserUser
import org.springframework.data.jpa.repository.Query
import kotlin.Long
import org.springframework.stereotype.Repository

@Repository
interface UserUserRepository : PassRepository<UserUser, Long>{
    @Query(
        """SELECT CASE WHEN COUNT(u) > 0 THEN TRUE ELSE FALSE END
            FROM UserUser u WHERE u.email = :email
        """
    )
    fun doesEmailExist(email: String): Boolean
}
