package ffufm.christine.api.repositories.user.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.christine.api.spec.dbo.user.UserAddress
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import java.util.*

@Repository
interface UserAddressRepository : PassRepository<UserAddress, Long> {
    @Query(
        "SELECT t from UserAddress t LEFT JOIN FETCH t.user",
        countQuery = "SELECT count(id) FROM UserAddress"
    )
    fun findAllAndFetchUser(pageable: Pageable): Page<UserAddress>
    @Query(
        """
            SELECT CASE WHEN COUNT(a) > 0 THEN TRUE ELSE FALSE END
            FROM UserAddress a WHERE a.street = :street 
            AND a.barangay = :barangay 
            AND a.city = :city 
            AND a.province = :province 
            AND a.zipCode = :zipCode
        """
    )
    fun doesAddressExist(street: String, barangay: String, city: String, province: String, zipCode: String): Boolean

    @Query(
        """
            SELECT a FROM UserAddress a
            LEFT JOIN FETCH a.user fn
            WHERE a.id = :id
        """
    )
    fun getAddressWithUser(id: Long): Optional<UserAddress>

    @Query(
        """
            SELECT a FROM UserAddress a
            LEFT JOIN FETCH a.user u
            WHERE u.id = :id
        """
    )
    fun getAllAddressesByUserId(id: Long): List<UserAddress>
}
