package ffufm.christine.api.repositories.user.user

import de.ffuf.pass.common.repositories.PassRepository
import ffufm.christine.api.spec.dbo.user.UserContactDetail
import kotlin.Long
import org.springframework.data.domain.Page
import org.springframework.data.domain.Pageable
import org.springframework.data.jpa.repository.Modifying
import org.springframework.data.jpa.repository.Query
import org.springframework.stereotype.Repository
import javax.transaction.Transactional

@Repository
interface UserContactDetailRepository : PassRepository<UserContactDetail, Long> {
    @Query(
        "SELECT t from UserContactDetail t LEFT JOIN FETCH t.user",
        countQuery = "SELECT count(id) FROM UserContactDetail"
    )
    fun findAllAndFetchUser(pageable: Pageable): Page<UserContactDetail>

    @Modifying
    @Transactional //
    @Query(
        """
           UPDATE UserContactDetail c SET c.isPrimary = false
           WHERE c.user.id = :userId
        """
    )
    fun updateIsPrimaryToFalse(userId: Long): Unit
    @Query(
        """
            SELECT COUNT (cd) FROM UserContactDetail
            cd WHERE cd.user.id = :userId
        """
    )
    fun countContactDetailsByUserId(userId: Long): Int
    @Query(
        """
           SELECT CASE WHEN COUNT(cd) > 0 THEN TRUE ELSE FALSE END
           FROM UserContactDetail cd WHERE cd.contactDetails = :contactDetails
        """
    )
    fun doesContactDetailExist(contactDetails: String): Boolean
//
//    @Query(
//        """
//            SELECT u FROM User u
//            LEFT JOIN FETCH u.UserContactDetail cd
//            WHERE u.id = :id
//        """
//    )
//    fun getAllContactDetailsByUserId(id: Long): List<UserContactDetail>
}
