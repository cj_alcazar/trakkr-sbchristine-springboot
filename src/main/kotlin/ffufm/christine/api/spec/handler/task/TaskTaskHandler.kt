package ffufm.christine.api.spec.handler.task

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.christine.api.spec.dbo.task.TaskTaskDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface TaskTaskDatabaseHandler {
    /**
     * Create Task: Creates a new Task object
     * HTTP Code 201: The created Task
     */
    suspend fun create(
        body: TaskTaskDTO,
        id: Long,
        projectId: Long
    ): TaskTaskDTO

    /**
     * Delete Task by id.: Deletes one specific Task.
     */
    suspend fun remove(id: Long)

    /**
     * Update the Task: Updates an existing Task
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    suspend fun update(body: TaskTaskDTO, id: Long): TaskTaskDTO
}

@Controller("task.Task")
class TaskTaskHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: TaskTaskDatabaseHandler

    /**
     * Create Task: Creates a new Task object
     * HTTP Code 201: The created Task
     */
    @RequestMapping(value = ["/users/{id:\\d+}/projects/{projectId:\\d+}/tasks/"], method =
            [RequestMethod.POST])
    suspend fun create(
        @RequestBody body: TaskTaskDTO,
        @PathVariable("id") id: Long,
        @PathVariable("projectId") projectId: Long
    ): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.create(body, id, projectId) }
    }

    /**
     * Delete Task by id.: Deletes one specific Task.
     */
    @RequestMapping(value = ["/tasks/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * Update the Task: Updates an existing Task
     * HTTP Code 200: The updated model
     * HTTP Code 404: The requested object could not be found by the submitted id.
     * HTTP Code 422: On or many fields contains a invalid value.
     */
    @RequestMapping(value = ["/tasks/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: TaskTaskDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
