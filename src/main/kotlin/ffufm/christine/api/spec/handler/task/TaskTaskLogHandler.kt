package ffufm.christine.api.spec.handler.task

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.christine.api.spec.dbo.task.TaskTaskLogDTO
import kotlin.Int
import kotlin.Long
import kotlin.String
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.data.domain.Page
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.bind.annotation.RequestParam
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface TaskTaskLogDatabaseHandler {
    /**
     * Get all TaskLogs by page by taskId: Returns all TaskLogs by taskId from the system that the
     * user has access to.
     * HTTP Code 200: 
     */
    suspend fun getAllLogsByTaskId(
        id: Long,
        maxResult: Int,
        page: Int
    ): Page<TaskTaskLogDTO>

    /**
     * Get all TaskLogs by page by userId: 
     * HTTP Code 200: 
     */
    suspend fun getAllLogsByUserId(
        id: String,
        maxResult: Int,
        page: Int
    ): Page<TaskTaskLogDTO>
}

@Controller("task.TaskLog")
class TaskTaskLogHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: TaskTaskLogDatabaseHandler

    /**
     * Get all TaskLogs by page by taskId: Returns all TaskLogs by taskId from the system that the
     * user has access to.
     * HTTP Code 200: 
     */
    @RequestMapping(value = ["/tasks/{id:\\d+}/tasklogs/"], method = [RequestMethod.GET])
    suspend fun getAllLogsByTaskId(
        @PathVariable("id") id: Long,
        @RequestParam("maxResult") maxResult: Int,
        @RequestParam("page") page: Int
    ): ResponseEntity<*> {

        return paging { databaseHandler.getAllLogsByTaskId(id, maxResult, page) }
    }

    /**
     * Get all TaskLogs by page by userId: 
     * HTTP Code 200: 
     */
    @RequestMapping(value = ["/users/{id:.*}/tasklogs/"], method = [RequestMethod.GET])
    suspend fun getAllLogsByUserId(
        @PathVariable("id") id: String,
        @RequestParam("maxResult") maxResult: Int,
        @RequestParam("page") page: Int
    ): ResponseEntity<*> {

        return paging { databaseHandler.getAllLogsByUserId(id, maxResult, page) }
    }
}
