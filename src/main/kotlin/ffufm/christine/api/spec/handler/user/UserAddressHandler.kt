package ffufm.christine.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.christine.api.spec.dbo.user.UserAddressDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserAddressDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    suspend fun createAddress(body: UserAddressDTO, id: Long): UserAddressDTO

    /**
     * : 
     * HTTP Code 200: Delete is successful.
     * HTTP Code 404: The object with the ID is not found
     */
    suspend fun remove(id: Long)

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    suspend fun update(body: UserAddressDTO, id: Long): UserAddressDTO
}

@Controller("user.Address")
class UserAddressHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserAddressDatabaseHandler

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    @RequestMapping(value = ["/users/addresses/{id:\\d+}/"], method = [RequestMethod.POST])
    suspend fun createAddress(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
                id.validateOrThrow()
        return success { databaseHandler.createAddress(body, id) }
    }

    /**
     * : 
     * HTTP Code 200: Delete is successful.
     * HTTP Code 404: The object with the ID is not found
     */
    @RequestMapping(value = ["/users/addresses/{id:\\d+}"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    @RequestMapping(value = ["/users/addresses/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun update(@RequestBody body: UserAddressDTO, @PathVariable("id") id: Long):
            ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.update(body, id) }
    }
}
