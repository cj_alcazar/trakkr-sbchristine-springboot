package ffufm.christine.api.spec.handler.user

import com.fasterxml.jackson.module.kotlin.readValue
import de.ffuf.pass.common.handlers.PassMvcHandler
import ffufm.christine.api.spec.dbo.user.UserContactDetailDTO
import kotlin.Long
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpStatus
import org.springframework.http.ResponseEntity
import org.springframework.stereotype.Controller
import org.springframework.web.bind.annotation.PathVariable
import org.springframework.web.bind.annotation.RequestBody
import org.springframework.web.bind.annotation.RequestMapping
import org.springframework.web.bind.annotation.RequestMethod
import org.springframework.web.multipart.MultipartFile
import org.springframework.web.multipart.MultipartHttpServletRequest
import org.springframework.web.server.ResponseStatusException

interface UserContactDetailDatabaseHandler {
    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: Object with the ID is not found
     */
    suspend fun createContactDetail(body: UserContactDetailDTO, id: Long): UserContactDetailDTO

    /**
     * : 
     * HTTP Code 200: Delete is successful.
     * HTTP Code 404: The object with the ID is not found
     */
    suspend fun remove(id: Long)

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    suspend fun updateContactDetails(body: UserContactDetailDTO, id: Long): UserContactDetailDTO
}

@Controller("user.ContactDetail")
class UserContactDetailHandler : PassMvcHandler() {
    @Autowired
    lateinit var databaseHandler: UserContactDetailDatabaseHandler

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: Object with the ID is not found
     */
    @RequestMapping(value = ["/users/{id:\\d+}/contact-details/"], method = [RequestMethod.POST])
    suspend fun createContactDetail(@RequestBody body: UserContactDetailDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.createContactDetail(body, id) }
    }

    /**
     * : 
     * HTTP Code 200: Delete is successful.
     * HTTP Code 404: The object with the ID is not found
     */
    @RequestMapping(value = ["/users/contact-details/{id:\\d+}/"], method = [RequestMethod.DELETE])
    suspend fun remove(@PathVariable("id") id: Long): ResponseEntity<*> {

        return success { databaseHandler.remove(id) }
    }

    /**
     * : 
     * HTTP Code 200: 
     * HTTP Code 404: The object with the ID is not found
     */
    @RequestMapping(value = ["/users/contact-details/{id:\\d+}/"], method = [RequestMethod.PUT])
    suspend fun updateContactDetails(@RequestBody body: UserContactDetailDTO, @PathVariable("id")
            id: Long): ResponseEntity<*> {
        body.validateOrThrow()
        return success { databaseHandler.updateContactDetails(body, id) }
    }
}
