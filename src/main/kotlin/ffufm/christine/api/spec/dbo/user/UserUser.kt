package ffufm.christine.api.spec.dbo.user

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.christine.api.spec.dbo.project.ProjectProject
import ffufm.christine.api.spec.dbo.project.ProjectProjectDTO
import ffufm.christine.api.spec.dbo.project.ProjectProjectSerializer
import ffufm.christine.api.spec.dbo.task.TaskTask
import ffufm.christine.api.spec.dbo.task.TaskTaskDTO
import ffufm.christine.api.spec.dbo.task.TaskTaskLog
import ffufm.christine.api.spec.dbo.task.TaskTaskLogDTO
import ffufm.christine.api.spec.dbo.task.TaskTaskLogSerializer
import ffufm.christine.api.spec.dbo.task.TaskTaskSerializer
import ffufm.christine.api.spec.dbo.user.UserAddressSerializer
import ffufm.christine.api.spec.dbo.user.UserContactDetailSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.Lob
import javax.persistence.OneToMany
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Boolean
import kotlin.Long
import kotlin.String
import kotlin.collections.List
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.ColumnDefault
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * User Model for the Trakkr API
 */
@Entity(name = "UserUser")
@Table(name = "user_user")
data class UserUser(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * firstName of the user
     * Sample: Brandon
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "first_name"
    )
    val firstName: String = "",
    /**
     * lastName of the user
     * Sample: Cruz
     */
    @Column(
        length = 50,
        updatable = true,
        nullable = false,
        name = "last_name"
    )
    val lastName: String = "",
    /**
     * if the user is still active
     * Sample: TRUE
     */
    @ColumnDefault("TRUE")
    @Column(name = "is_active")
    val isActive: Boolean = false,
    /**
     * Type off user: GC = General Contractor, SC = Sub Contractor, CR = Contractor
     * Sample: GC
     */
    @Column(
        length = 2,
        updatable = true,
        nullable = false,
        name = "user_type"
    )
    val userType: String = "",
    /**
     * email of the user
     * Sample: brandoncruz@gmail.com
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "email"
    )
    @Lob
    val email: String = "",
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    val contactDetails: List<UserContactDetail>? = mutableListOf(),
    @OneToMany(mappedBy = "assignee", fetch = FetchType.LAZY)
    val tasks: List<TaskTask>? = mutableListOf(),
    @OneToMany(mappedBy = "taskUserRelationship", fetch = FetchType.LAZY)
    val taskLogs: List<TaskTaskLog>? = mutableListOf(),
    @OneToMany(mappedBy = "owner", fetch = FetchType.LAZY)
    val projects: List<ProjectProject>? = mutableListOf(),
    @OneToMany(mappedBy = "user", fetch = FetchType.LAZY)
    val addresses: List<UserAddress>? = mutableListOf()
) : PassDTOModel<UserUser, UserUserDTO, Long>() {
    override fun toDto(): UserUserDTO = super.toDtoInternal(UserUserSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserUser, UserUserDTO, Long>, UserUserDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()

    enum class Usertype(
        val value: String
    ) {
        CONTRACTOR("CR"),

        GENERALCONTRACTOR("GC"),

        SUBCONTRACTOR("SC");
    }
}

/**
 * User Model for the Trakkr API
 */
data class UserUserDTO(
    val id: Long? = null,
    /**
     * firstName of the user
     * Sample: Brandon
     */
    val firstName: String? = "",
    /**
     * lastName of the user
     * Sample: Cruz
     */
    val lastName: String? = "",
    /**
     * if the user is still active
     * Sample: TRUE
     */
    val isActive: Boolean? = false,
    /**
     * Type off user: GC = General Contractor, SC = Sub Contractor, CR = Contractor
     * Sample: GC
     */
    val userType: String? = "",
    /**
     * email of the user
     * Sample: brandoncruz@gmail.com
     */
    val email: String? = "",
    val contactDetails: List<UserContactDetailDTO>? = null,
    val tasks: List<TaskTaskDTO>? = null,
    val taskLogs: List<TaskTaskLogDTO>? = null,
    val projects: List<ProjectProjectDTO>? = null,
    val addresses: List<UserAddressDTO>? = null
) : PassDTO<UserUser, Long>() {
    override fun toEntity(): UserUser = super.toEntityInternal(UserUserSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<UserUser, PassDTO<UserUser, Long>, Long>,
            PassDTO<UserUser, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class UserUserSerializer : PassDtoSerializer<UserUser, UserUserDTO, Long>() {
    override fun toDto(entity: UserUser): UserUserDTO = cycle(entity) {
        UserUserDTO(
                id = entity.id,
        firstName = entity.firstName,
        lastName = entity.lastName,
        isActive = entity.isActive,
        userType = entity.userType,
        email = entity.email,
        contactDetails = entity.contactDetails?.toSafeDtos(),
        tasks = entity.tasks?.toSafeDtos(),
        taskLogs = entity.taskLogs?.toSafeDtos(),
        projects = entity.projects?.toSafeDtos(),
        addresses = entity.addresses?.toSafeDtos()
                )}

    override fun toEntity(dto: UserUserDTO): UserUser = UserUser(
            id = dto.id,
    firstName = dto.firstName ?: "",
    lastName = dto.lastName ?: "",
    isActive = dto.isActive ?: false,
    userType = dto.userType ?: "",
    email = dto.email ?: "",
    contactDetails = dto.contactDetails?.toEntities() ?: emptyList(),
    tasks = dto.tasks?.toEntities() ?: emptyList(),
    taskLogs = dto.taskLogs?.toEntities() ?: emptyList(),
    projects = dto.projects?.toEntities() ?: emptyList(),
    addresses = dto.addresses?.toEntities() ?: emptyList()
            )
    override fun idDto(id: Long): UserUserDTO = UserUserDTO(
            id = id,
    firstName = null,
    lastName = null,
    isActive = null,
    userType = null,
    email = null,

            )}

@Service("user.UserUserValidator")
class UserUserValidator : PassModelValidation<UserUser> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserUser>):
            ValidatorBuilder<UserUser> = validatorBuilder.apply {
        konstraint(UserUser::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUser::lastName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUser::userType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(2)
        }
    }
}

@Service("user.UserUserDTOValidator")
class UserUserDTOValidator : PassModelValidation<UserUserDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<UserUserDTO>):
            ValidatorBuilder<UserUserDTO> = validatorBuilder.apply {
        konstraint(UserUserDTO::firstName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUserDTO::lastName) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(50)
        }
        konstraint(UserUserDTO::userType) {
            notNull()
            @Suppress("MagicNumber")
            lessThanOrEqual(2)
        }
    }
}
