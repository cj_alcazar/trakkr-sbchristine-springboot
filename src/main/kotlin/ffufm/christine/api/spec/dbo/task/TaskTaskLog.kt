package ffufm.christine.api.spec.dbo.task

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.christine.api.spec.dbo.user.UserUser
import ffufm.christine.api.spec.dbo.user.UserUserDTO
import ffufm.christine.api.spec.dbo.user.UserUserSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Logs of actions done that involves Task.
 */
@Entity(name = "TaskTaskLog")
@Table(name = "task_tasklog")
data class TaskTaskLog(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Log description of taskLog
     * Sample: User x created Task x under Project x
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "log"
    )
    @Lob
    val log: String = "",
    /**
     * id of the task
     * Sample: 1
     */
    @Column(name = "task_id")
    val taskId: Long? = null,
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    val taskUserRelationship: UserUser? = null
) : PassDTOModel<TaskTaskLog, TaskTaskLogDTO, Long>() {
    override fun toDto(): TaskTaskLogDTO = super.toDtoInternal(TaskTaskLogSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TaskTaskLog, TaskTaskLogDTO, Long>,
            TaskTaskLogDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Logs of actions done that involves Task.
 */
data class TaskTaskLogDTO(
    val id: Long? = null,
    /**
     * Log description of taskLog
     * Sample: User x created Task x under Project x
     */
    val log: String? = "",
    /**
     * id of the task
     * Sample: 1
     */
    val taskId: Long? = null,
    val taskUserRelationship: UserUserDTO? = null
) : PassDTO<TaskTaskLog, Long>() {
    override fun toEntity(): TaskTaskLog = super.toEntityInternal(TaskTaskLogSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TaskTaskLog, PassDTO<TaskTaskLog, Long>, Long>,
            PassDTO<TaskTaskLog, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class TaskTaskLogSerializer : PassDtoSerializer<TaskTaskLog, TaskTaskLogDTO, Long>() {
    override fun toDto(entity: TaskTaskLog): TaskTaskLogDTO = cycle(entity) {
        TaskTaskLogDTO(
                id = entity.id,
        log = entity.log,
        taskId = entity.taskId,
        taskUserRelationship = entity.taskUserRelationship?.idDto() ?:
                entity.taskUserRelationship?.toDto()
                )}

    override fun toEntity(dto: TaskTaskLogDTO): TaskTaskLog = TaskTaskLog(
            id = dto.id,
    log = dto.log ?: "",
    taskId = dto.taskId,
    taskUserRelationship = dto.taskUserRelationship?.toEntity()
            )
    override fun idDto(id: Long): TaskTaskLogDTO = TaskTaskLogDTO(
            id = id,
    log = null,
    taskId = null,

            )}

@Service("task.TaskTaskLogValidator")
class TaskTaskLogValidator : PassModelValidation<TaskTaskLog> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TaskTaskLog>):
            ValidatorBuilder<TaskTaskLog> = validatorBuilder.apply {
    }
}

@Service("task.TaskTaskLogDTOValidator")
class TaskTaskLogDTOValidator : PassModelValidation<TaskTaskLogDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TaskTaskLogDTO>):
            ValidatorBuilder<TaskTaskLogDTO> = validatorBuilder.apply {
    }
}
