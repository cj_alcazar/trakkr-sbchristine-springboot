package ffufm.christine.api.spec.dbo.task

import am.ik.yavi.builder.ValidatorBuilder
import am.ik.yavi.builder.konstraint
import am.ik.yavi.builder.konstraintOnObject
import de.ffuf.pass.common.models.PassDTO
import de.ffuf.pass.common.models.PassDTOModel
import de.ffuf.pass.common.models.PassDtoSerializer
import de.ffuf.pass.common.models.PassModelValidation
import de.ffuf.pass.common.models.idDto
import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.utilities.extensions.konstraint
import de.ffuf.pass.common.utilities.extensions.toEntities
import de.ffuf.pass.common.utilities.extensions.toSafeDtos
import ffufm.christine.api.spec.dbo.project.ProjectProject
import ffufm.christine.api.spec.dbo.project.ProjectProjectDTO
import ffufm.christine.api.spec.dbo.project.ProjectProjectSerializer
import ffufm.christine.api.spec.dbo.user.UserUser
import ffufm.christine.api.spec.dbo.user.UserUserDTO
import ffufm.christine.api.spec.dbo.user.UserUserSerializer
import java.util.TreeSet
import javax.persistence.Column
import javax.persistence.Entity
import javax.persistence.FetchType
import javax.persistence.GeneratedValue
import javax.persistence.GenerationType
import javax.persistence.Id
import javax.persistence.Index
import javax.persistence.JoinColumn
import javax.persistence.Lob
import javax.persistence.ManyToOne
import javax.persistence.SequenceGenerator
import javax.persistence.Table
import javax.persistence.UniqueConstraint
import kotlin.Long
import kotlin.String
import kotlin.reflect.KClass
import org.hibernate.annotations.CacheConcurrencyStrategy
import org.hibernate.annotations.FetchMode
import org.springframework.beans.factory.getBeansOfType
import org.springframework.stereotype.Component
import org.springframework.stereotype.Service

/**
 * Tasks of the user
 */
@Entity(name = "TaskTask")
@Table(name = "task_task")
data class TaskTask(
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    var id: Long? = null,
    /**
     * Name of the task
     * Sample: Task Name
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "name"
    )
    @Lob
    val name: String = "",
    /**
     * Description of the task
     * Sample: Sample Description
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "description"
    )
    @Lob
    val description: String = "",
    /**
     * Current status of the task
     * Sample: DONE
     */
    @Column(
        nullable = false,
        updatable = true,
        name = "status"
    )
    @Lob
    val status: String = "",
    @ManyToOne(fetch = FetchType.LAZY, optional = false)
    @JoinColumn(nullable = false)
    val taskProjectRelationship: ProjectProject? = null,
    @ManyToOne(fetch = FetchType.LAZY, optional = true)
    @JoinColumn(nullable = true)
    val assignee: UserUser? = null
) : PassDTOModel<TaskTask, TaskTaskDTO, Long>() {
    override fun toDto(): TaskTaskDTO = super.toDtoInternal(TaskTaskSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TaskTask, TaskTaskDTO, Long>, TaskTaskDTO, Long>>)

    override fun readId(): Long? = this.id

    override fun toString(): String = super.toString()
}

/**
 * Tasks of the user
 */
data class TaskTaskDTO(
    val id: Long? = null,
    /**
     * Name of the task
     * Sample: Task Name
     */
    val name: String? = "",
    /**
     * Description of the task
     * Sample: Sample Description
     */
    val description: String? = "",
    /**
     * Current status of the task
     * Sample: DONE
     */
    val status: String? = "",
    val taskProjectRelationship: ProjectProjectDTO? = null,
    val assignee: UserUserDTO? = null
) : PassDTO<TaskTask, Long>() {
    override fun toEntity(): TaskTask = super.toEntityInternal(TaskTaskSerializer::class as
            KClass<PassDtoSerializer<PassDTOModel<TaskTask, PassDTO<TaskTask, Long>, Long>,
            PassDTO<TaskTask, Long>, Long>>)

    override fun readId(): Long? = this.id
}

@Component
class TaskTaskSerializer : PassDtoSerializer<TaskTask, TaskTaskDTO, Long>() {
    override fun toDto(entity: TaskTask): TaskTaskDTO = cycle(entity) {
        TaskTaskDTO(
                id = entity.id,
        name = entity.name,
        description = entity.description,
        status = entity.status,
        taskProjectRelationship = entity.taskProjectRelationship?.idDto() ?:
                entity.taskProjectRelationship?.toDto(),
        assignee = entity.assignee?.idDto() ?: entity.assignee?.toDto()
                )}

    override fun toEntity(dto: TaskTaskDTO): TaskTask = TaskTask(
            id = dto.id,
    name = dto.name ?: "",
    description = dto.description ?: "",
    status = dto.status ?: "",
    taskProjectRelationship = dto.taskProjectRelationship?.toEntity(),
    assignee = dto.assignee?.toEntity()
            )
    override fun idDto(id: Long): TaskTaskDTO = TaskTaskDTO(
            id = id,
    name = null,
    description = null,
    status = null,

            )}

@Service("task.TaskTaskValidator")
class TaskTaskValidator : PassModelValidation<TaskTask> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TaskTask>):
            ValidatorBuilder<TaskTask> = validatorBuilder.apply {
        konstraintOnObject(TaskTask::taskProjectRelationship) {
            notNull()
        }
    }
}

@Service("task.TaskTaskDTOValidator")
class TaskTaskDTOValidator : PassModelValidation<TaskTaskDTO> {
    override fun buildValidator(validatorBuilder: ValidatorBuilder<TaskTaskDTO>):
            ValidatorBuilder<TaskTaskDTO> = validatorBuilder.apply {
        konstraintOnObject(TaskTaskDTO::taskProjectRelationship) {
            notNull()
        }
    }
}
