package ffufm.christine.api.spec.handler.user.implementation

import ffufm.christine.api.PassTestBase
import ffufm.christine.api.repositories.user.user.UserAddressRepository
import ffufm.christine.api.spec.dbo.user.UserAddress
import ffufm.christine.api.spec.handler.user.UserAddressDatabaseHandler
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserAddressDatabaseHandlerTest : PassTestBase() {
    @Autowired
    lateinit var userAddressDatabaseHandler: UserAddressDatabaseHandler



    @Test
    fun `deleteAddress should success`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = userAddressRepository.save(
            EntityGenerator.createAddress().copy( user = user ))

        userAddressDatabaseHandler.remove(address.id!!)

        assertEquals(0, userContactDetailRepository.findAll().count())
    }

    @Test
    fun `deleteAddress should fail given invalid addressId`() = runBlocking {
        val invalidId : Long = 123
        val exception = assertFailsWith<ResponseStatusException> {
            userAddressDatabaseHandler.remove(invalidId)
        }
        val expectedException = "404 NOT_FOUND \"UserAddress with ID $invalidId not found\""

        assertEquals(expectedException, exception.message)

    }

    @Test
    fun `updateAddress should return updated addresses given valid inputs`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = userAddressDatabaseHandler.createAddress(
            EntityGenerator.createAddress().copy(user = user).toDto(), user.id!!)

        val body = address.copy(
            street = "Paz",
            barangay = "900"
        )
        val updatedAddress = userAddressDatabaseHandler.update(body, address.id!!)
        assertEquals(body.city, updatedAddress.city)
        assertEquals(body.barangay, updatedAddress.barangay)
    }



    @Test
    fun `createAddress should return user with addresses`() = runBlocking {

        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = EntityGenerator.createAddress().copy(user = user)
        val createdAddress = userAddressDatabaseHandler.createAddress(address.toDto(),user.id!!)

        assertEquals(createdAddress.street, address.street)
        assertEquals(createdAddress.barangay, address.barangay)
        assertEquals(createdAddress.city, address.city)
        assertEquals(createdAddress.province, address.province)
        assertEquals(createdAddress.zipCode, address.zipCode)

        assertEquals(1, userAddressRepository.findAll().count())
    }

}
