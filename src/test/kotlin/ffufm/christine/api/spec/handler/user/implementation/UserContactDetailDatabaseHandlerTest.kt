package ffufm.christine.api.spec.handler.user.implementation

import ffufm.christine.api.PassTestBase
import ffufm.christine.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.web.server.ResponseStatusException
import kotlin.test.assertEquals
import kotlin.test.assertFailsWith

class UserContactDetailDatabaseHandlerTest : PassTestBase() {


    @Autowired
    lateinit var userContactDetailDatabaseHandler: UserContactDetailDatabaseHandler


    @Test
    fun `create contactDetail should return correct isPrimary value`() = runBlocking {
        // 1. Create a contact detail (isPrimary = true)
        val savedUser = userUserRepository.save(EntityGenerator.createUser())

        val bodyContactDetail = EntityGenerator.createContactDetails()
        val firstContactDetail = userContactDetailRepository.save(
            bodyContactDetail.copy(
                user = savedUser
            )
        )
        assertEquals(true, firstContactDetail.isPrimary)

        // 2. Create another contactDetail (isPrimary =  missing)
        // Test if the second contactDetail (isPrimary = false)
        val secondContactDetail = userContactDetailDatabaseHandler.createContactDetail(bodyContactDetail.copy(
            contactDetails = "09127183333",
            isPrimary = false,
            user = savedUser
        ).toDto(), savedUser.id!!)

        assertEquals(false, secondContactDetail.isPrimary)

        // 3. Create another contactDetail (isPrimary = true)
        val thirdContactDetail = userContactDetailDatabaseHandler.createContactDetail(bodyContactDetail.copy(
            contactDetails = "090071433333",
            isPrimary = true,
            user = savedUser
        ).toDto(), savedUser.id!!)
        assertEquals(true, thirdContactDetail.isPrimary)
        assertEquals(false, userContactDetailRepository.findById(firstContactDetail.id!!).get().isPrimary)
    }

    @Test
    fun `create contactDetail should throw an error given 4th contactDetail`() = runBlocking {
        //1. Create a contactDetails (isPrimary = true)
        val user = userUserRepository.save(EntityGenerator.createUser())
        val body = EntityGenerator.createContactDetails().copy(user = user)

        // Create 3 contact details
        userContactDetailRepository.saveAll(
            listOf(
                body,
                body.copy(contactDetails = "0912321324", isPrimary = false,user = user ),
                body.copy(contactDetails = "0942342432", isPrimary = false,user = user)

            )
        )
        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.createContactDetail(
                body.copy(
                    contactDetails = "0912345678",
                    isPrimary = false,
                    user = user
                ).toDto(), user.id!!
            )
        }

        val expectedException = "400 BAD_REQUEST \"You cannot create more than four details.\""

        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create should return contact details`() = runBlocking {
        // Check if the repository is empty.
        assertEquals(0, userContactDetailRepository.findAll().count())

        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetails = EntityGenerator.createContactDetails().copy(user = user)

        val createdContactDetails = userContactDetailDatabaseHandler.createContactDetail(contactDetails.toDto(), user.id!!)
        assertEquals(1, userContactDetailRepository.findAll().count())
        assertEquals(contactDetails.contactDetails, createdContactDetails.contactDetails)
        assertEquals(contactDetails.contactType, createdContactDetails.contactType)

    }

    @Test
    fun `create should fail given duplicate contact details`() = runBlocking {


        val user = userUserRepository.save(EntityGenerator.createUser())

        val contactDetail = userContactDetailRepository.save(EntityGenerator.createContactDetails().copy(user = user))

        val duplicateContactDetails = contactDetail.copy(
            contactDetails = "09238383838",
            contactType = "Home Contact"
        )

        // Test if there's an error
        val exception = assertFailsWith<ResponseStatusException> {
            userContactDetailDatabaseHandler.createContactDetail(duplicateContactDetails.toDto(), user.id!!)
        }

        val expectedException =
            "409 CONFLICT \"Contact Detail ${duplicateContactDetails.contactDetails} already exists!\""
        assertEquals(expectedException, exception.message)
    }

    @Test
    fun `create contact details should return contact details given valid id`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails().copy(user = user)


        val contactserviceImpl = userContactDetailDatabaseHandler.createContactDetail(contactDetail.toDto(), user.id!!)

        assertEquals(user.id, contactserviceImpl.user!!.id)
        assertEquals(contactDetail.contactDetails, contactserviceImpl.contactDetails)
        assertEquals(contactDetail.contactType, contactserviceImpl.contactType)

    }




    @Test
    fun `delete contact details should work`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails()
        val createdContactDetails = userContactDetailRepository.save(contactDetail.copy(user = user))

        assertEquals(1, userContactDetailRepository.findAll().count())
        userContactDetailDatabaseHandler.remove(createdContactDetails.id!!)
        assertEquals(0, userContactDetailRepository.findAll().count())
    }

    @Test
    fun `update contact details should return updated contact details given valid inputs`() = runBlocking {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contactDetail = EntityGenerator.createContactDetails()
        val original = userContactDetailRepository.save(contactDetail.copy(user = user))

        val body = original.copy(
            contactDetails = "09668569897",
            contactType = "Mobile"
        )

        val updatedContactDetails = userContactDetailRepository.updateIsPrimaryToFalse(original.id!!)
        assertEquals(body.isPrimary, true)

    }
}