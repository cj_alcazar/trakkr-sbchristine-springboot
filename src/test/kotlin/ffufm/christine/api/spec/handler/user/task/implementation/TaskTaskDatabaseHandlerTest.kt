package ffufm.christine.api.spec.handler.user.task.implementation

import ffufm.christine.api.PassTestBase
import ffufm.christine.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.christine.api.spec.handler.task.TaskTaskDatabaseHandler
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class TaskTaskDatabaseHandlerTest : PassTestBase() {

    @Autowired
    lateinit var taskTaskDatabaseHandler: TaskTaskDatabaseHandler

    @Autowired
    lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler

    @Test
    fun `create should return task`() = runBlocking {


        assertEquals(0, taskTaskRepository.findAll().count())

        val user = userUserDatabaseHandler.create(EntityGenerator.createUser().toDto())

        val project = projectProjectDatabaseHandler.create(
            EntityGenerator.createProject(), user.id!!
        )

        val task = EntityGenerator.createTask()
        val createdTask = taskTaskDatabaseHandler.create(task, user.id!!, project.id!!)

        assertEquals(task.name, createdTask.name)
        assertEquals(task.description, createdTask.description)
        assertEquals(task.status, createdTask.status)
        assertEquals(1, taskTaskRepository.findAll().count())
    }




}