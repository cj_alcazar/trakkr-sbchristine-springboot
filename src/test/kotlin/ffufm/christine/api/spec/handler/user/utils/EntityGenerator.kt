package ffufm.christine.api.spec.handler.user.utils

import ffufm.christine.api.spec.dbo.project.ProjectProjectDTO
import ffufm.christine.api.spec.dbo.task.TaskTaskDTO
import ffufm.christine.api.spec.dbo.user.UserAddress
import ffufm.christine.api.spec.dbo.user.UserContactDetail
import ffufm.christine.api.spec.dbo.user.UserUser
import ffufm.christine.api.utils.UserTypeEnum


object EntityGenerator {

    fun createUser(): UserUser = UserUser(
        firstName = "Brandon",
        lastName = "Cruz",
        email = "brandon@brandon.com",
        userType = UserTypeEnum.CR.value
    )

    fun createContactDetails(): UserContactDetail = UserContactDetail(
        contactDetails = "09238383838",
        contactType = "Home"
    )

    fun createAddress(): UserAddress = UserAddress(
        street = "Morning Star",
        barangay = "Culiat",
        city = "Quezon City",
        province = "Metro Manila",
        zipCode = "1028"
    )
    fun createProject(): ProjectProjectDTO = ProjectProjectDTO(
        name = "Company A Project",
        description = "For a new room",
        status = "ON-GOING"
    )

    fun createTask(): TaskTaskDTO = TaskTaskDTO(
        name = "Company A Project",
        description = "For a new room",
        status = "DONE"
    )
}