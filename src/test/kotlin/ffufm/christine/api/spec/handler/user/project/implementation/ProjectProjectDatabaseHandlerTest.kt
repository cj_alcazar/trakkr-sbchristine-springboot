package ffufm.juveil.api.spec.handler.user.project.implementation

import ffufm.juveil.api.PassTestBase
import ffufm.juveil.api.spec.dbo.project.ProjectProject
import ffufm.juveil.api.spec.handler.project.ProjectProjectDatabaseHandler
import ffufm.juveil.api.spec.handler.user.UserContactDetailDatabaseHandler
import ffufm.juveil.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import kotlin.test.assertEquals

class ProjectProjectDatabaseHandlerTest : PassTestBase() {

    @Autowired
    lateinit var projectProjectDatabaseHandler: ProjectProjectDatabaseHandler

    @Test
    fun `create should return project`() = runBlocking {
        assertEquals(0, projectProjectRepository.findAll().count())

        val user = EntityGenerator.create()
        val createdUser = userUserDatabaseHandler.create(user.toDto())

        val project = EntityGenerator.createProject()
        val createdProject = projectProjectDatabaseHandler.create(project, createdUser.id!!)

        assertEquals(project.name, createdProject.name)
        assertEquals(project.description, createdProject.description)
        assertEquals(project.status, createdProject.status)
        assertEquals(1, projectProjectRepository.findAll().count())


    }

    @Test
    fun `test update project`() = runBlocking {
        val owner = userUserRepository.save(EntityGenerator.create())
        val project = projectProjectRepository.save(
            EntityGenerator.createProject().toEntity().copy(owner = owner)
        )

        val body = project.copy(
            status = "COMPLETED"
        )

        val updatedProject = projectProjectDatabaseHandler.update(body.toDto(), project.id!!)
        assertEquals(body.status, updatedProject.status)
    }

    @Test
    fun `delete should work`() = runBlocking {
        val project = EntityGenerator.createProject()
        val original = projectProjectRepository.save(project.toEntity()).toDto()

        assertEquals(1, projectProjectRepository.findAll().count())
        projectProjectDatabaseHandler.remove(original.id!!)
        assertEquals(0, projectProjectRepository.findAll().count())
    }
}