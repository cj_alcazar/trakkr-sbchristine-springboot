package ffufm.christine.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.christine.api.PassTestBase
import ffufm.christine.api.repositories.user.user.UserAddressRepository
import ffufm.christine.api.spec.dbo.user.UserAddress
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserAddressHandlerTest : PassTestBase() {


    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser
    fun `test remove`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = userAddressRepository.save(
            EntityGenerator.createAddress().copy(user = user)
        )
                mockMvc.delete("/users/addresses/{id}", address.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk ()}

                }
    }

    @Test
    @WithMockUser
    fun `test update`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = userAddressRepository.save(
            EntityGenerator.createAddress().copy(user = user)
        )

        val body = address.copy(
            street = "Paz",
            barangay = "900"
        )
        mockMvc.put("/users/addresses/{id}/", address.id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }

                }
    }
    @Test
    @WithMockUser
    fun `createAddress should return isOk`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val address = EntityGenerator.createAddress().copy(user = user)

        mockMvc.post("/users/address/{id}/", user.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(address)
        }.asyncDispatch().andExpect {
            status { isOk() }

        }
    }

    @Test
    @WithMockUser
    fun `test createAddress`() {
        val id: Long = 0
                mockMvc.post("/users/addresses/{id}/", id) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk() }

                }
    }
}
