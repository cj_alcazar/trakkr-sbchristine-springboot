package ffufm.christine.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.christine.api.PassTestBase
import ffufm.christine.api.repositories.user.user.UserContactDetailRepository
import ffufm.christine.api.spec.dbo.user.UserContactDetail
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator
import org.hamcrest.CoreMatchers
import org.junit.After
import org.junit.Before
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.HttpHeaders
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserContactDetailHandlerTest : PassTestBase() {


    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc


    @Test
    @WithMockUser
    fun `test createContactDetail`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val body =EntityGenerator.createContactDetails()

                mockMvc.post("/users/{id}/contact-details/", user.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }
                }
    }

    @Test
    @WithMockUser
    fun `test updateContactDetails`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contact = userContactDetailRepository.save(
            EntityGenerator.createContactDetails().copy( user = user )
        )

        val body = contact.copy(
            contactDetails = "09264069192",
            contactType = "Mobile",
        )
                mockMvc.put("/users/contact-details/{id}/", contact.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON
                    content = objectMapper.writeValueAsString(body)
                }.andExpect {
                    status { isOk() }

                }
    }

    @Test
    @WithMockUser
    fun `test remove`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        val contact = userContactDetailRepository.save(
            EntityGenerator.createContactDetails().copy(user = user)
        )
                mockMvc.delete("/users/contact-details/{id}/", contact.id!!) {
                    accept(MediaType.APPLICATION_JSON)
                    contentType = MediaType.APPLICATION_JSON

                }.andExpect {
                    status { isOk() }

                }
    }

}
