package ffufm.christine.api.spec.handler.user.project.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.christine.api.PassTestBase
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator

import org.junit.jupiter.api.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.*

class ProjectProjectHandlerTest : PassTestBase() {
    @Autowired
    private lateinit var objectMapper: ObjectMapper

    @Autowired
    private lateinit var mockMvc: MockMvc

    @Test
    @WithMockUser
    fun `test project`() {
        val createdUser = userUserRepository.save(EntityGenerator.createUser())
        val body = EntityGenerator.createProject()
        mockMvc.post("/users/{id}/projects/", createdUser.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.asyncDispatch().andExpect {
            status { isOk() }

        }
    }
//
//
//    @Test
//    @WithMockUser
//    fun `test getAll`() {
//        val body = EntityGenerator.createProject()
//        val users = projectProjectRepository.saveAll(
//            listOf(
//                body,
//                body.copy(name = "Project Name 1"),
//                body.copy(name = "Project Name 2"),
//                body.copy(name = "Project Name 3"),
//                body.copy(name = "Project Name 4")
//
//            )
//
//        )
//        val page = 0
//        val maxResult = 2
//        val totalPages = users.count() / maxResult
//
//        mockMvc.get("/projects/?page=$page&maxResult=$maxResult") {
//            accept(MediaType.APPLICATION_JSON)
//            contentType = MediaType.APPLICATION_JSON
//
//        }.asyncDispatch().andExpect {
//            status { isOk() }
//            jsonPath("$.totalElements") {
//                value(users.count())
//
//            }
//            jsonPath("$.numberOfElements") { value(maxResult) }
//            jsonPath("$.totalPages") { value(totalPages) }
//        }
//    }
//
//    @org.junit.Test
//    @WithMockUser
//    fun `delete should work`() {
//        val project = projectProjectRepository.save(EntityGenerator.createProject().toEntity())
//        mockMvc.delete("/projects/{id}/", project.id!!) {
//            accept(MediaType.APPLICATION_JSON)
//            contentType = MediaType.APPLICATION_JSON
//
//        }.asyncDispatch().andExpect {
//            status { isOk() }
//
//        }
//    }
//
//    @org.junit.Test
//    @WithMockUser
//    fun `update user should return isOk`() {
//        val project = projectProjectRepository.save(EntityGenerator.createProject().toEntity())
//        val body = project.copy(
//            name = "Project Name",
//            description = "Sample Description",
//            status = "ON-GOING"
//        )
//
//
//        mockMvc.put("/projects/{id}/", project.id!!) {
//            accept(MediaType.APPLICATION_JSON)
//            contentType = MediaType.APPLICATION_JSON
//            content = objectMapper.writeValueAsString(body)
//        }.asyncDispatch().andExpect {
//            status { isOk() }
//
//        }
//    }

//    @Test
//    @WithMockUser
//    fun `delete project should return 404 if invalid id`() {
//        val invalidId : Long = 111
//
//
//
//        mockMvc.delete("/projects/{id}/", invalidId) {
//            accept(MediaType.APPLICATION_JSON)
//            contentType = MediaType.APPLICATION_JSON
//
//        }.asyncDispatch().andExpect {
//            status { isNotFound() }
//        }
//    }

}