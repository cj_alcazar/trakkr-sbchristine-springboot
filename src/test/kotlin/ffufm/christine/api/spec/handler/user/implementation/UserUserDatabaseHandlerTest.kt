package ffufm.christine.api.spec.handler.user.implementation

import ffufm.christine.api.PassTestBase
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator
import kotlinx.coroutines.runBlocking
import org.junit.Test
import kotlin.test.assertEquals

class UserUserDatabaseHandlerTest : PassTestBase() {




    @Test
    fun `test create`() = runBlocking {
        // Check if the repository is empty.
        assertEquals(0, userUserRepository.findAll().count())

        val user = EntityGenerator.createUser()


        val createdUser = userUserDatabaseHandler.create(user.toDto())
        // Actual tests
        assertEquals(1, userUserRepository.findAll().count())
        assertEquals(user.firstName, createdUser.firstName)
        assertEquals(user.lastName, createdUser.lastName)
        assertEquals(user.email, createdUser.email)
        assertEquals(user.userType, createdUser.userType)
    }

    @Test
    fun `test getById should return a user`() = runBlocking {
        val user = EntityGenerator.createUser()

        val savedUser = userUserRepository.save(user)
        userUserDatabaseHandler.getById(savedUser.id!!)
        assertEquals(1, userUserRepository.findAll().size)
    }
    @Test
    fun `test remove`() = runBlocking {
        val user = EntityGenerator.createUser()

        val savedUser = userUserRepository.save(user)

        assertEquals(1, userUserRepository.findAll().count())

        userUserDatabaseHandler.remove(savedUser.id!!)
        assertEquals(0, userUserRepository.findAll().count())
    }

    @Test
    fun `test update`() = runBlocking {
        val user = EntityGenerator.createUser()

        val createdUser = userUserDatabaseHandler.create(user.toDto())
        val body = createdUser.copy(
            firstName = "Juveil",
            lastName = "Batalla",
        )

        val updatedUser = userUserDatabaseHandler.update(body, createdUser.id!!)
        assertEquals(body.firstName, updatedUser.firstName)
        assertEquals(body.lastName, updatedUser.lastName)
        assertEquals(body.email, updatedUser.email)



    }

    @Test
    fun `create should return user`() = runBlocking {

        // Check if the repository is empty.
        assertEquals(0, userUserRepository.findAll().count())

        val user = EntityGenerator.createUser()

        val createdUser = userUserDatabaseHandler.create(user.toDto())
        // Actual tests
        assertEquals(1, userUserRepository.findAll().count())
        assertEquals(user.firstName, createdUser.firstName)
        assertEquals(user.lastName, createdUser.lastName)
        assertEquals(user.email, createdUser.email)
        assertEquals(user.userType, createdUser.userType)

    }
    @Test
    fun `test getAll`() = runBlocking {
        val body = EntityGenerator.createUser()
        userUserRepository.saveAll(
            listOf(
                body,
                body.copy(email = "Brenda@brenda.com")
            )
        )
        val users = userUserDatabaseHandler.getAll(0,100)
        assertEquals(2, users.count())
    }


}
