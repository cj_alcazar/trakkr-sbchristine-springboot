package ffufm.christine.api.spec.handler.user.integration

import com.fasterxml.jackson.databind.ObjectMapper
import ffufm.christine.api.PassTestBase
import ffufm.christine.api.spec.handler.user.utils.EntityGenerator
import org.junit.Test
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.http.MediaType
import org.springframework.security.test.context.support.WithMockUser
import org.springframework.test.web.servlet.MockMvc
import org.springframework.test.web.servlet.delete
import org.springframework.test.web.servlet.get
import org.springframework.test.web.servlet.post
import org.springframework.test.web.servlet.put

class UserUserHandlerTest : PassTestBase() {

    @Autowired
     lateinit var objectMapper: ObjectMapper

    @Autowired
     lateinit var mockMvc: MockMvc



    @Test
    @WithMockUser
    fun `test create`() {
        val body = EntityGenerator.createUser()
        mockMvc.post("/users/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `test getById`() {
        val body = EntityGenerator.createUser()
        val user = userUserRepository.save(body)

        mockMvc.get("/users/{id}/", user.id!!) {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON

        }.andExpect {
            status { isOk() }
        }
    }
    @Test
    @WithMockUser
    fun `create user should return 200`() {

        val body = EntityGenerator.createUser()

        mockMvc.post("/users/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isOk() }
        }
    }


    @Test
    @WithMockUser
    fun `create user should return 409 if duplicate email`() {
        userUserRepository.save(EntityGenerator.createUser())

        val body = EntityGenerator.createUser()

        mockMvc.post("/users/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {
            status { isConflict() }
        }
    }

    @Test
    @WithMockUser
    fun `create user should return 404 if userType is valid`() {
        val user = userUserRepository.save( EntityGenerator.createUser().copy(
            userType = "AA"
        ))

        mockMvc.post("/users/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(user)
        }.andExpect { status { isConflict() }
        }
    }

    @Test
    @WithMockUser
    fun `get user should return user`() {
        val user = userUserRepository.save(EntityGenerator.createUser())
        mockMvc.get("/users/${user.id!!}/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `update user should return 200`() {

        val user = EntityGenerator.createUser()
        val savedUser = userUserRepository.save(user)
        val body = user.copy(
            firstName = "Brenda",
            lastName = "Mage",
            email = "Brenda@Brenda.com"
        )
        mockMvc.put("/users/${savedUser.id!!}/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
            content = objectMapper.writeValueAsString(body)
        }.andExpect {status { isOk() }
        }
    }
    @Test
    @WithMockUser
    fun `delete user should return 200`() {
        val savedUser = userUserRepository.save(EntityGenerator.createUser())

        mockMvc.delete("/users/${savedUser.id!!}/") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {status { isOk() }
        }
    }

    @Test
    @WithMockUser
    fun `get all users should have correct pagination`(){
        val body = EntityGenerator.createUser()
        val users = userUserRepository.saveAll(
            listOf(
                body,
                body.copy(email = "Brenda@gmail.com"),
                body.copy(email = "Brando@gmail.com"),
                body.copy(email = "bren@gmail.com"),
                body.copy(email = "brad@gmail.com"),
                body.copy(email = "bro@gmail.com")
            )
        )
        val page = 0
        val maxResult = 2
        val totalPages = users.count() / maxResult
        mockMvc.get("/users/?page=$page&maxResult=$maxResult") {
            accept(MediaType.APPLICATION_JSON)
            contentType = MediaType.APPLICATION_JSON
        }.andExpect {
            status { isOk() }
        }

    }
}
