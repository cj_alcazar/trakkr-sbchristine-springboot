package ffufm.christine.api

import de.ffuf.pass.common.security.SpringContext
import de.ffuf.pass.common.security.SpringSecurityAuditorAware
import ffufm.christine.api.handlerimpl.user.user.UserUserHandlerImpl
import ffufm.christine.api.repositories.project.ProjectProjectRepository
import ffufm.christine.api.repositories.task.TaskTaskRepository
import ffufm.christine.api.repositories.user.user.UserAddressRepository
import ffufm.christine.api.repositories.user.user.UserContactDetailRepository
import ffufm.christine.api.repositories.user.user.UserUserRepository
import ffufm.christine.api.spec.handler.user.UserUserDatabaseHandler
import org.junit.After
import org.junit.Before
import org.junit.runner.RunWith
import org.springframework.beans.factory.annotation.Autowired
import org.springframework.boot.test.autoconfigure.web.servlet.AutoConfigureMockMvc
import org.springframework.boot.test.context.SpringBootTest
import org.springframework.context.ApplicationContext
import org.springframework.test.context.ActiveProfiles
import org.springframework.test.context.junit4.SpringRunner

@RunWith(SpringRunner::class)
@ActiveProfiles("test")
@SpringBootTest(classes = [SBChristine::class, SpringSecurityAuditorAware::class])
@AutoConfigureMockMvc
abstract class PassTestBase {
    @Autowired
    lateinit var userUserRepository: UserUserRepository

    @Autowired
    lateinit var projectProjectRepository: ProjectProjectRepository

    @Autowired
    lateinit var userContactDetailRepository: UserContactDetailRepository

    @Autowired
    lateinit var userAddressRepository: UserAddressRepository

    @Autowired
    lateinit var taskTaskRepository: TaskTaskRepository

    @Autowired
    lateinit var userUserDatabaseHandler: UserUserDatabaseHandler



    @Autowired
    lateinit var context: ApplicationContext


    @Before
    fun initializeContext() {
        SpringContext.context = context
    }
    @After
    fun cleanRepositories(){

        taskTaskRepository.deleteAll()
        projectProjectRepository.deleteAll()
        userAddressRepository.deleteAll()
        userContactDetailRepository.deleteAll()
        userUserRepository.deleteAll()

    }
}
